<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'test' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', '' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'mY$0o7+sf,q7GhZ/&jAOnZ65exFLT&8i*X#tr#8kW&~5M3$pMRqqnO>s/HFsDl$h' );
define( 'SECURE_AUTH_KEY', '#FbV 7U[^UY3tt*lu([;r%vp40.[64][/,6U!7&uu3Zuc:HU3-qC`#T}DerEhX1S' );
define( 'LOGGED_IN_KEY', 'oR62RD9_By2xnex>d(hH5c{v6k@Da=nxI@,n^p)_>WNeM&eM#v9Rw?8OVU.hj0S8' );
define( 'NONCE_KEY', '-~nxED@on$JLJb82Cc_dMsiCd^qx~Lh+.qW|v):DEwSyZjd@>`p1~BCik+3+)bN;' );
define( 'AUTH_SALT', 'oSw])h{j9*g*AYRVxB+@/D38$yFvj!*b!1*@m:U=E9It:nLXJF7L2/+g)iX1Ojw2' );
define( 'SECURE_AUTH_SALT', 'Pu(e2@gVk4k?.3f|CA]D@1.Zphj~tPC!-v[4P?K}+Hkl6vsk#{:sDx{#z:J):% h' );
define( 'LOGGED_IN_SALT', 'am$AK;!32,f{[3A<-`d_dKM!GW(.z3~yidPBRYDypl>2Wz%T;?Us##:8Dqo(0TL*' );
define( 'NONCE_SALT', 'Plj}{+h)5U79vks`^yfZ+~Y4kZ(Y+D$?M+&WD[$;qDmUP130>@>[7ZBxd8|=}|&N' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

