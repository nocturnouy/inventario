<?php
/*
 Template Name: Home Fotos
*/
?>

<?php get_header(); ?>

	<div id="home-container">

		<?php
			$args = array('post_type' => 'custom_type');
			
			// The Query
			$the_query = new WP_Query( $args );
			 
			// The Loop
			if ( $the_query->have_posts() ) {
			    while ( $the_query->have_posts() ) {
			        $the_query->the_post();
			        $custom = get_post_custom();
			        $fixed = $custom['fixphoto'][0];
			        $highlight = $custom['highlight'][0];


			        echo '<a class="foto-thumb-link" href="#"';
					if($fixed==='true') {
					    echo 'data-fixed="true"';
					}else{
						echo 'data-fixed="false"';
					}
					if($highlight==='true') {
					    echo 'data-highlight="true"';
					}else{
						echo 'data-highlight="false"';
					}
			        echo 'data-featherlight="#'.get_the_ID().'" data-featherlight-iframe-allowfullscreen="true" >';
			        echo get_the_post_thumbnail( $post_id, 'foto-home', array( 'class' => 'home-image' ) );
			        echo '</a>';
			    }
			} else {
			    // no posts found
			    echo 'no se encontraron imagenes';
			}
			/* Restore original Post Data */
			wp_reset_postdata();
		?>
	</div>
	<div class="post-contents">
		<?php
			$args = array('post_type' => 'custom_type');
			
			// The Query
			$the_query = new WP_Query( $args );
			 
			// The Loop
			if ( $the_query->have_posts() ) {
			    while ( $the_query->have_posts() ) {
			        $the_query->the_post();
			        $custom = get_post_custom();
			        //$fixed = $custom['fixphoto'][0];
			        echo '<div id="'.get_the_ID().'">';
			        echo '<div class="text-bar"><h3>'.get_the_title().'</h3>';
			        if(isset($custom['location'])) {
					    echo '<a href="https://www.google.com/maps/search/?api=1&query='.$custom['location'][0].'" target="_blank">'.$custom['location'][0].'</a></div>';
					}
			        echo get_the_post_thumbnail( $post_id, 'foto-home', array( 'class' => 'home-image' ) );
			        echo '</div>';
			    }
			} else {
			    // no posts found
			    echo 'no se encontraron imagenes';
			}
			/* Restore original Post Data */
			wp_reset_postdata();
		?>
	</div>
	<script type="text/javascript">
		function generateRandomInteger(min, max) {
		  return Math.floor(min + Math.random()*(max + 1 - min))
		}
		jQuery(document).ready(function($){
		// standard on load code goes here with $ prefix
		// note: the $ is setup inside the anonymous function of the ready command
		$('.foto-thumb-link').each(function(){
				if ( $(this).attr('data-highlight') == 'true' ){
					var scale = generateRandomInteger(120,140)/100
					var translateX = generateRandomInteger(0,0);
					var translateY = generateRandomInteger(0,0);
				}else{
					var scale = generateRandomInteger(60,100)/100;
					var translateX = generateRandomInteger(-50,50);
					var translateY = generateRandomInteger(-100,100);
				}
				if ( $(this).attr('data-fixed') == 'true' ){
					$(this).attr('style', 'z-index:99')
				}else{
					$(this).attr('style', 'transform:scale(' + scale + ') translateX(' + translateX + 'px) translateY(' + translateY + 'px)')
				}
			})
		});

		
	</script>
<?php get_footer(); ?>
