<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>
<style type="text/css">
	
	/*single post styles*/

	header.header {
	    display: none;
	}

	footer.footer {
	    display: none;
	}

	section.foto-container {
	    width: 70%;
	    display: inline-block;
	    vertical-align: top;
	}

	aside.foto-description {
	    width: 29%;
	    display: inline-block;
	}

	section.foto-container img {
	    width: 100%;
	    height: auto;
	}

	div#wpadminbar {
	    display: none;
	}
	html {
		margin-top:0 !important;
	}


</style>
<div id="foto-main">						
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<section class="foto-container"><?php the_post_thumbnail('fotos-main'); ?></section>
			<aside class="foto-description">
				<h1 class="single-title"><?php the_title(); ?></h1>
				
			
				<?php the_content();
					$custom = get_post_custom();

					if(isset($custom['location'])) {
					    echo '<a href="https://www.google.com/maps/search/?api=1&query='.$custom['location'][0].'" target="_blank">location</a>';
					}
				?>

			</aside>
				


		<?php endwhile; ?>


		<?php endif; ?>

</div>

<?php get_footer(); ?>
